<?php 

/**
 * function generate table result
 */
function views_count_form() {
  $header = array(t('name'), t('title'), t('count'));
  $rows = array();
  $query = db_select('views_count')
    ->fields(NULL, array('name', 'title', 'count'))
    ->orderBy('name')
    ->execute();
  while ($value = $query->fetchAssoc()) {
    $rows[] = array(
      $value['name'],
      $value['title'],
      $value['count'],
    );
  }
  return theme('table', array('header' => $header, 'rows' => $rows));
}