<?php

/**
 * create admin form
 */
function popup_sys_message_form() {
  $form = array();
  $form['head-color'] = array(
    '#type' => 'textfield',
    '#title' => 'Enter color title in HEX',
    '#description' => 'Color format: #FFFFFF',
    '#default_value' => variable_get('head-color'),
  );
  $form['body-color'] = array(
    '#type' => 'textfield',
    '#title' => 'Enter color conteiner in HEX',
    '#description' => 'Color format: #FFFFFF',
    '#default_value' => variable_get('body-color'),
  );
  $form['text-color'] = array(
    '#type' => 'textfield',
    '#title' => 'Enter color background text in HEX',
    '#description' => 'Color format: #FFFFFF',
    '#default_value' => variable_get('text-color'),
  );
  return system_settings_form($form);
}