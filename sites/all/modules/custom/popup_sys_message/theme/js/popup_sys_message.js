// create custom function draggable

(function($){

  Drupal.behaviors.popup_sys_message = {
    attach: function(context, settings) {
      $.fn.adcidrag = function(options){
        var settings = $.extend({ 
          handle: 0, 
          exclude: 0 
        }, options);
          return this.each(function(){
        var dx, dy, el = $(this), handle = settings.handle ? $(settings.handle, el) : el;
        handle.on({
          mousedown: function(e){
            if (settings.exclude && ~$.inArray(e.target, $(settings.exclude, el))) {
              return;
            } 
          e.preventDefault();
          var os = el.offset(); dx = e.pageX-os.left, dy = e.pageY-os.top;
          $(document).on('mousemove.drag', function(e){ 
            el.offset({
              top: e.pageY-dy, 
              left: e.pageX-dx}); 
          });
          },
          mouseup: function(e){
            $(document).off('mousemove.drag'); 
          }
        });
      });
    };
    $(document).ready(function($) {
      $('#messages').show(function(){
      $('body').append('<div id="overlay-message">');
      $('#messages').wrapAll('<div id="popup-window">');
      $('#messages').wrapAll('<div id="popup-head">');
      $('#messages').before('<div id="popup-close">x</div>');
      $('#popup-window').adcidrag({exclude: '#popup-head, #popup-close, #message, .section, .messages, .element-invisible'});
      $('#popup-close, #overlay-message').click(function(){
      $('#popup-window')
        .animate({opacity:0, top: '45%'},200,
          function(){
            $(this).css('display', 'none');
            $('#overlay-message').fadeOut(400);
          });
      });
      });
    });
    $(document).ready(function($) {
      if (!$('#popup-window','#popup-head','.messages')) {
        return 0;
      } 
        else if (isEmpty(Drupal.settings.popup_sys_message.color_head || Drupal.settings.popup_sys_message.color_body || 
        Drupal.settings.popup_sys_message.color_text)) {
          return 0;
        }
          else {
            $('#popup-head').css('background', Drupal.settings.popup_sys_message.color_head);
            $('#popup-window').css('background', Drupal.settings.popup_sys_message.color_body);
            $('.messages').css('background', Drupal.settings.popup_sys_message.color_text);
          }
    });

  }
};
}(jQuery));

